import Home from "./pages/Home";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./pages/Dashboard/LayoutDashboard";
import Add from "./pages/addNewCars";
import Detail from "./pages/Details";
import Invoice from "./pages/Invoice";
import "antd/dist/antd.css";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="dashboard" element={<Layout />} />
          <Route path="list" element={<Layout />} />
          <Route path="/list/:id" element={<Detail />} />
          <Route path="add" element={<Add />} />
          <Route path="invoice" element={<Invoice />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
