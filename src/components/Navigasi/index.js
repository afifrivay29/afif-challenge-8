import React from "react";

const Navigasi = () => {
  return (
    <nav className="flex ml-6 my-6" aria-label="Breadcrumb">
      <ol className="inline-flex items-center space-x-1 md:space-x-3">
        <li className="inline-flex items-center">
          <a href="/" className="text-black font-semibold hover:text-gray-900 inline-flex items-center">
            Home
          </a>
        </li>
        <li>
          <div className="flex items-center">
            <img src="/chevron-right.png" alt="chevron right" />
            <a href="list" className="text-black font-semibold hover:text-gray-900 ml-1 md:ml-2 text-sm ">
              List Cars
            </a>
          </div>
        </li>
        <li aria-current="page">
          <div className="flex items-center">
            <img src="/chevron-right.png" alt="chevron right" />

            <span className="text-black ml-1 md:ml-2 text-sm font-thin">Add New Car</span>
          </div>
        </li>
      </ol>
    </nav>
  );
};

export default Navigasi;
