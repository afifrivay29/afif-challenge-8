import React from "react";
import { MenuOutlined } from "@ant-design/icons";

const Navbar = () => {
  return (
    <div className="flex justify-between">
      <MenuOutlined style={{ color: "black", marginTop: "25px", fontSize: "20px", marginLeft: "24px" }} />
      <div className="mr-8 my-6 flex mt-5">
        <img src="/Search.png" alt="search" className="h-9 w-auto mr-7" />
        <img src="/fi_profile.png" alt="profile" className="rounded-full h-8 w-8" />
        <p className="font-light ml-2 text-lg">Rivaykusnanto</p>
      </div>
    </div>
  );
};

export default Navbar;
