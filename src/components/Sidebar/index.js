import React from "react";
import { useLocation } from "react-router-dom";

const Sidebar = () => {
  const location = useLocation();
  console.log(location.pathname);

  return (
    <div className="w-1/12 h-1000 bg-blue-900">
      <div className=" bg-[#CFD4ED] w-10 h-10 mx-auto my-4 "></div>
      {location.pathname === "/dashboard" ? (
        <>
          <a href="dashboard" className=" bg-[#e5e5e5] bg-opacity-40 block py-2">
            <img src="/fi_home.png" alt="home" className="h-7 w-7 mx-auto" />
            <p className=" font-semibold text-center text-white">Dashboard</p>
          </a>
          <a href="list" className="block py-2">
            <img src="/fi_truck.png" alt="truck" className="h-7 w-7 mx-auto" />
            <p className=" font-semibold text-center text-white">Cars</p>
          </a>
        </>
      ) : (
        <>
          <a href="dashboard" className="block py-2">
            <img src="/fi_home.png" alt="home" className="h-8 w-8 mx-auto" />
            <p className="font-semibold text-center text-white">Dashboard</p>
          </a>
          <a href="list" className="bg-[#e5e5e5] bg-opacity-40 block py-2">
            <img src="/fi_truck.png" alt="truck" className="h-8 w-8 mx-auto" />
            <p className="font-semibold text-center text-white">Cars</p>
          </a>
        </>
      )}
    </div>
  );
};

export default Sidebar;
