import React from "react";
// import ModalImage from "react-modal-image";

function CardDetail({ carData }) {
  return (
    <div className="rounded-lg w-[23rem] my-2 mx-2">
      <div className="flex flex-col">
        <div className="mx-auto">
          {/* <ModalImage small={carData.image} large={carData.image} alt="innova " /> */}
          {/* <img src="/innova.png" alt="innova" width="200" /> */}
        </div>
        <div className="ml-6">
          <p className="font-bold mt-6">
            {carData.name}/{carData.category}
          </p>
          <div className="flex text-grayFont text-sm">
            <div className="flex mr-2">
              <img src="/fi_users_card.svg" alt="icon users" width="12" />
              <p className="ml-2 font-light text-xs mt-2">4 Orang</p>
            </div>

            <div className="flex mr-2">
              <img src="/fi_settings.svg" alt="icon users" width="12" />
              <p className="ml-1 font-light text-xs mt-2">Manual</p>
            </div>

            <div className="flex">
              <img src="/fi_calendar_card.svg" alt="icon users" width="12" />
              <p className="ml-1 font-light text-xs mt-2">2020</p>
            </div>
          </div>

          <div className="flex justify-between">
            <p className="font-light mt-8">Total</p>
            <p className="font-bold mt-8 mr-6">Rp {carData.price}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CardDetail;
