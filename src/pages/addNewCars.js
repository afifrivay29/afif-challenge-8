import React from "react";
import Sidebar from "../components/Sidebar";
import SidebarTitle from "../components/SidebarTitle";
import Navbar from "../components/Navbar";
import InputCars from "../components/InputCars";
import Navigasi from "../components/Navigasi";

const Add = () => {
  return (
    <div className="flex">
      <Sidebar />
      <SidebarTitle />
      <div className="w-9/12 h-20 flex flex-col justify-between">
        <Navbar />
        <div className="w-full min-h-[88.9vh] bg-[#f4f5f7]">
          <Navigasi />
          <h2 className="font-bold text-lg ml-6 mb-4">Add New Car</h2>
          <InputCars />
        </div>
      </div>
    </div>
  );
};

export default Add;
