import React from "react";
import Header from "../components/Header";
import Ticket from "../components/Tiket";

const Invoice = () => {
  return (
    <div>
      <Header />
      <Ticket />
    </div>
  );
};

export default Invoice;
