import React from "react";
import "./Style.css";

const Home = () => {
  return (
    <div>
      <link rel="stylesheet" href="https://unpkg.com/swiper@8/swiper-bundle.min.css" />
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />
      <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container">
          <a className="navbar-brand" href="/"></a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <a className="nav-link" href="#our-services">
                  Our Services <span className="sr-only">(current)</span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#why-us">
                  Why Us
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#testimonial">
                  Testimonial
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#faq">
                  FAQ
                </a>
              </li>
              <li className="nav-item">
                <button className="btn btn-success tombol" href="#">
                  Register
                </button>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="container-fluid hero">
        <div className="container">
          <div className="row">
            <div className="col hero-section">
              <div className="row">
                <div className="col-lg hero-section-left">
                  <h2>Sewa & Rental Mobil Terbaik di kawasan Tangerang</h2>
                  <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                  <a href="/dashboard" className="btn btn-success tombol">
                    Mulai Sewa Mobil
                  </a>
                </div>
                <div className="col-lg hero-section-right">
                  <img src="/img_car.png" alt="car image" className="img-fluid" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container" id="why-us">
        <div className=" why-us-title">
          <h3>Why Us?</h3>

          <p>Mengapa harus pilih Binar Car Rental?</p>
        </div>

        <div className="row why-us-card">
          <div className="col">
            <div className="card">
              <img src="/icon_complete.svg" width="32" alt="icon_price" />
              <div className="card-body">
                <h4>Mobil Lengkap</h4>
                <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="card">
              <img src="/icon_price.svg" width="32" alt="icon_price" />
              <div className="card-body">
                <h4>Harga Murah</h4>
                <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="card">
              <img src="/icon_24hrs.svg" width="32" alt="icon_price" />
              <div className="card-body">
                <h4>Layanan 24 Jam</h4>
                <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="card">
              <img src="/icon_professional.svg" width="32" alt="icon_price" />
              <div className="card-body">
                <h4>Sopir Profesional</h4>
                <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container-fluid" id="our-services">
        <div className="row testimonial-title ">
          <h2>Data Visualization</h2>
          
          <h4>Data Statistik Untuk Sewa Mobil Terlaris 2022</h4>
        </div>
      </div>


      <div className="container-fluid" id="testimonial">
        <div className="row testimonial-title ">
          <h3>Testimonial</h3>
          <p>Berbagai review positif dari para pelanggan kami</p>
        </div>
      </div>

      <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    </div>
  );
};

export default Home;
