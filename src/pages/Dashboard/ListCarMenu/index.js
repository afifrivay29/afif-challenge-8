import React from "react";
import Card from "../../../components/Card";
import { useState, useEffect } from "react";
import axios from "axios";

const ListCar = () => {
  const [car, setCars] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {
    const fetchCar = async () => {
      setLoading(true);
      try {
        const res = await axios.get(`https://rent-cars-api.herokuapp.com/admin/car`);
        setCars(res.data);
        console.log(res.data);
        setLoading(false);
      } catch (error) {
        console.error(error);
        setError(true);
        setLoading(false);
      }
    };
    fetchCar();
  }, []);
  return (
    <div className="h-full w-full">
      <div className="px-6 bg-[#f4f5f7] min-h-full pb-10">
        <p className="font-bold  py-4">
          Cars <img src="/chevron-right.png" className="inline" /> <span className=" font-thin">List Car</span>
        </p>
        <div className="flex justify-between">
          <p className="text-xl font-bold my-2">List Car</p>
          <a href="add" className="py-2 bg-primary-darkblue5 px-2 text-white font-semibold mt-1">
            <span>
              <img src="fi_plus.png" alt="icon plus" className="inline mr-3 mb-1" />
            </span>
            Add New Car
          </a>
        </div>
        {/* Button */}
        <div className="my-6">
          <a href="#" className="text-primary-darkblue4 font-bold px-2 bg-primary-darkblue1 py-2 rounded border-2 border-primary-darkblue4 mr-2">
            All
          </a>

          <a href="#" className="text-primary-darkblue2 font-bold px-2 bg-white py-2 rounded border-2 border-primary-darkblue2 mr-2">
            Small
          </a>

          <a href="#" className="text-primary-darkblue2 font-bold px-2 bg-white py-2 rounded border-2 border-primary-darkblue2 mr-2">
            Medium
          </a>

          <a href="#" className="text-primary-darkblue2 font-bold px-2 bg-white py-2 rounded border-2 border-primary-darkblue2 mr-2">
            Large
          </a>
        </div>

        {/* Card */}
        <div className="flex flex-wrap justify-start ">
          {loading && <div className="w-20 h-20 justify-center border-8 border-blue-400 border-solid rounded-full animate-spin border-t-white"></div>}
          {error && <p>Error...</p>}
          {car.length > 0 && <Card carsData={car} />}
        </div>
      </div>
    </div>
  );
};

export default ListCar;
