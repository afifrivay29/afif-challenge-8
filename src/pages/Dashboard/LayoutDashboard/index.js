import React from "react";
import ListCar from "../ListCarMenu";
import Admin from "../Admin";
import Navbar from "../../../components/Navbar";
import Sidebar from "../../../components/Sidebar";
import SidebarTitle from "../../../components/SidebarTitle";
import { useLocation } from "react-router-dom";

const Layout = () => {
  const location = useLocation();
  return (
    <div className="flex">
      <Sidebar />
      <SidebarTitle />
      <div className="w-9/12 h-20 bg-white flex flex-col justify-between">
        <Navbar />
        {location.pathname === "/dashboard" ? <Admin /> : <ListCar />}
      </div>
    </div>
  );
};

export default Layout;
