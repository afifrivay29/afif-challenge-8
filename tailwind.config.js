module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: {
          darkblue1: "#CFD4ED",
          darkblue2: "#AEB7E1",
          darkblue3: "#5E70C4",
          darkblue4: "#0D28A6",
          darkblue5: "#091B6F",
          limegreen1: "#DEF1DF",
          limegreen2: "#C9E7CA",
          limegreen3: "#92D094",
          limegreen4: "#5CB85F",
          limegreen5: "#3D7B3F",
        },
        alert: {
          success: "#73CA5C",
          warning: "#F9CC00",
          danger: "#FA2C5A",
        },
        neutral: {
          neutral1: "#FFFFFF",
          neutral2: "#D0D0D0",
          neutral3: "#8A8A8A",
          neutral4: "#3C3C3C",
          neutral5: "#151515",
        },
      },
    },
  },
  plugins: [],
};
